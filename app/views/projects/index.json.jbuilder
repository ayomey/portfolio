json.array!(@projects) do |project|
  json.extract! project, :id, :title, :content, :picture
  json.url project_url(project, format: :json)
end
